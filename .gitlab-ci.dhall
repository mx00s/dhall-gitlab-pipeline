let GitLab =
      https://raw.githubusercontent.com/bgamari/dhall-gitlab-ci/a5fd0cee99b0b4515a3cb2ab43ca4e1aa23190cb/package.dhall

let Prelude = GitLab.Prelude

let renderTop = GitLab.Top.toJSON

let demoJob =
      GitLab.Job::{
      , stage = Some "build"
      , image = Some "alpine:latest"
      , script = [ "echo 'Hello World'" ]
      }

let top = GitLab.Top::{ jobs = toMap { generated-job = demoJob } }

in  Prelude.JSON.renderYAML (renderTop top)
