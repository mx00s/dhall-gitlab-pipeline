# Dhall GitLab Pipeline

GitLab's CI/CD pipelines are usually configured using a
[domain-specific
language](https://docs.gitlab.com/ee/ci/yaml/README.html) embedded in
YAML (`.gitlab-ci.yml`). The DSL is complex, untyped, evolving, and
provides [limited validation
capabilities](https://docs.gitlab.com/ee/ci/yaml/README.html#validate-the-gitlab-ciyml).

GitLab 12.9 introduced [dynamic child
pipelines](https://docs.gitlab.com/ee/ci/parent_child_pipelines.html#dynamic-child-pipelines)
which means the primary pipeline controlled by `gitlab-ci.yml` can
dynamically create a `generated-config.yml` file to spawn a child
pipeline. This repository explores using a configuration language
called [Dhall](https://dhall-lang.org/) to generate the child pipeline
configuration.

## Guidelines

`.gitlab-ci.yml` is intended to be as small as possible. Its only
responsibilities are to

1. validate Dhall expression
1. convert the `.gitlab-ci.dhall` file into `generated-config.yml`
1. spawn a dynamic child pipeline

All other CI/CD configuration belongs in `.gitlab-ci.dhall`.

Ideally the `generated-config.yml` should be self-contained. Dhall's
features enable and encourage writing DRY configs and make it simple
to compose and normalize arbitrarily large expressions. Pipeline
configuration features like
[`include`](https://docs.gitlab.com/ee/ci/yaml/README.html#include)
can be replaced Dhall's ability to treat local file paths and URLs as
Dhall expressions.

## Future improvements

- GitLab pipeline DSL validation (moving target)
  - [ ] implement a user-friendly schema to reduce the risk of
        generating a semantically invalid `generated-config.yml`.
	- Ben Gamari's
      [dhall-gitlab-ci](https://github.com/bgamari/dhall-gitlab-ci)
      project has already started on this and the demo uses it via
      URLs pinned to an early git revision.
- optimizations
  - [ ] commit `generated-config.yml`, optimistically assume it's in
        sync to start child pipeline faster, and if it's out of sync
        with `.gitlab-ci.dhall` render the diff and fail the job.
  - [ ] create a recommended pre-push hook to help users keep the
        committed `generated-config.yml` in sync with
        `.gitlab-ci.dhall`.
- [ ] multiple dynamic child pipelines (see
      [limitations](https://docs.gitlab.com/ee/ci/parent_child_pipelines.html#limitations))
